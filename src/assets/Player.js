class Player{
    /**
     * Create a PlayerProp
     * @param pseudo le pseudo du joueur
     * @param fullname les noms prénoms du joueur
     * @param age l'âge du joueur
     * @param uuid l'id correspondant au compte minecraft du joueur (pour récuperer son skin)
     */
    constructor({ pseudo='', fullname='', age = '',uuid = '' }) {
        this.pseudo = pseudo
        this.fullname = fullname
        this.age = age
        this.uuid = uuid
    }


    clone(){
        return new Player({
            'pseudo': this.pseudo,
            'fullname': this.fullname,
            'age': this.age,
            'uuid' : this.uuid,
        })
    }
}

export { Player }
