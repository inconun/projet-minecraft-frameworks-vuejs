const servers = [
    {
        "name": "Invivables",
        "ip": "109.238.10.40"
    },
    {
        "name": "Master 2 FI",
        "ip": "minecraft-server.servegame.com"
    },
    {
        "name": "Ulycraft",
        "ip": "play.ulycraft.fr"
    },
    {
        "name": "Game of Arkadia",
        "ip": "play.gameofarkadia.net:27120"
    },
    {
        "name" : "Harion",
        "ip" : "harion.fr"
    },
    {
        "name" : "oc tc",
        "ip" : "eu.oc.tc"
    }
]

export { servers }
