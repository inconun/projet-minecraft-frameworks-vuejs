import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export default new VueRouter({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: () => import ("../components/Home")
        },
        {
            path:"/online",
            name:"online",
            component: () => import ("../components/OnlinePlayers")
        },
        {
            path:"/player",
            name:"player",
            component: () => import ("../components/PlayerCard")
        }

    ]
})
